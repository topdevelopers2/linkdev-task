// init app environment .
const AppEnvironment = require('./src/util/env/AppEnvironment');
AppEnvironment.init();

// init db connection.
const DbConnection = require("./src/data/db/DbConnection");
DbConnection.connect();

// init redis cache connection.
const AppCache = require('./src/data/cache/AppCache');
AppCache.connect();

// Register App Events Listener
const EventObserver = require("./src/util/EventObserver");
const Events = require('./src/constant/Events');
const MailSender = require('./src/http/integration/MailSender');
EventObserver.getInstance().subscribe(Events.SEND_VERIFICATION_EMAIL, MailSender.sendEmail);

// init express app & register routes and start listening.
const express = require('express');
const app = express();
require('./src/http/routes/index')(app);
const AppLogger = require("./src/util/log/AppLogger");
const AppSocket = require("./src/http/socket/AppSocket");

const port = AppEnvironment.getEnv().port;
const server = app.listen(port, () => {
    AppLogger.log(`app listening at http://localhost:${port}`);
});
AppSocket.getInstance().startListening(server);
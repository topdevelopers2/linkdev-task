# Task Documentation
code organized based on 2 type of packaging:-
* Packaging by layer - like http , data packages.
* Packaging by feature - like auth feature in controllers package.

## Some of design patterns used.
* Singleton in Db connection instance class.
* Observer Pattern in EventObserver class.
* Builder Pattern in HttpResponse class.

### i always hide third party libraries in classes and methods by applying 
[Independent of framework pattern] for easy changing in libraries and hide complexity of library.

### Improvements
* send email in new Thread with worker_threads package , or use another concept like rabbit MQ.

const apiV1Router = require("./v1/index");
const exceptionHandler = require('../middleware/exceptionHandler');
const AppLogger = require("../../util/log/AppLogger");
const bodyParser = require('body-parser');
const cors = require("cors");
const AppEnvironment = require("../../util/env/AppEnvironment");
const ApiResponse = require("../../util/response/ApiResponse");

module.exports = (appServer) => {
    if (AppEnvironment.getEnv().isTestEnv()) {
        appServer.use(cors());
    }
    appServer.use(bodyParser.json());

    // register api v1 routes.
    appServer.use("/api/v1", apiV1Router);
    // catch all not found routes.
    appServer.all("*", (req, res) => {
        return ApiResponse.sendFail(req, res, "Url not found");
    });
    appServer.use(exceptionHandler);

    // catch all not handled exceptions.
    process.on("unhandledRejection", (error, p) => {
        AppLogger.logError(error);
        //TODO send email with failure reason.
        process.exit(1);
    });
};
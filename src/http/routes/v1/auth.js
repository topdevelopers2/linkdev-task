const AuthController = require('../../controllers/v1/auth/AuthController');
const mustBeLoginMiddleware = require('../../middleware/mustBeLoginMiddleware');

module.exports = (router) => {
    const authController = new AuthController();
    router.post("/auth/register", authController.register);
    router.post("/auth/login", authController.login);
    router.put("/auth/emailVerification", authController.verifyEmail);
    router.get("/auth/profile",[mustBeLoginMiddleware], authController.getProfile);
};
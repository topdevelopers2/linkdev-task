const ApiResponseCode = require("../../constant/ApiResponseCode");
const BaseException = require("./BaseException");

class NotAuthorizedException extends BaseException {
    constructor(message, statusCode = ApiResponseCode.NOT_AUTHORIZED, serverCode = 401) {
        super(message, statusCode, serverCode);
    }
}

module.exports = NotAuthorizedException;
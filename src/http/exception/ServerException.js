const BaseException = require("./BaseException");
const ApiResponseCode = require("../../constant/ApiResponseCode");

class ServerException extends BaseException {
    constructor(message, statusCode = ApiResponseCode.SERVER_ERROR, serverCode = 500) {
        super(message, statusCode, serverCode);
    }
}

module.exports = ServerException;
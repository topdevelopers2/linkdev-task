const BaseException = require('./BaseException');
const ApiResponseCode = require("../../constant/ApiResponseCode");

class BadRequestException extends BaseException {
    constructor(message, statusCode = ApiResponseCode.BAD_REQUEST, serverCode = 400) {
        super(message, statusCode, serverCode);
    }
}

module.exports = BadRequestException;
const ApiResponseCode = require("../../constant/ApiResponseCode");
const BaseException = require("./BaseException");

class ValidationException extends BaseException {
    constructor(message, statusCode = ApiResponseCode.VALIDATION_ERROR, serverCode = 412) {
        super(message, statusCode, serverCode);
    }
}

module.exports = ValidationException;
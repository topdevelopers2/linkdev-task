const NotAuthorizedException = require("../exception/NotAuthorizedException");
const JwtHelper = require("../../util/JwtHelper");
module.exports = async function (req, res, next) {
    try {
        const authHeader = req.headers["authorization"];
        const token = authHeader && authHeader.split(" ")[1];
        if (!token) {
            throw new NotAuthorizedException("User not authorized");
        }
        req.user = await JwtHelper.verify(token);
        next();
    } catch (e) {
        next(e);
    }
}
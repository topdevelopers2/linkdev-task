const BaseException = require("../exception/BaseException");
const AppLogger = require("../../util/log/AppLogger");
const ApiResponseCode = require("../../constant/ApiResponseCode");
const ApiResponse = require("../../util/response/ApiResponse");

module.exports = function (err, req, res, next) {
    return ApiResponse.sendException(req, res, err);
}
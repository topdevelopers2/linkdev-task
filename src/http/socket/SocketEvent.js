module.exports = {
    CONNECT: "connection",
    DISCONNECT: "disconnect",
    NEW_CONNECTION: "new_connection",
    NEW_MESSAGE: "new_message",
    LIVE_USERS: "live_users",
    USER_DISCONNECT: "user_disconnected",
    MESSAGES: "messages",
    INVALID_TOKEN: "invalid_token"
}
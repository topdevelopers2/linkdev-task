const AppLogger = require("../../util/log/AppLogger");
const MessageCache = require("../../data/cache/MessageCache");
const JwtHelper = require("../../util/JwtHelper");
const MessageRepository = require("../../data/repository/MessageRepository");
const Message = require("../../data/model/Message");
const socket = require("socket.io");
const SocketEvent = require('./SocketEvent');

module.exports = class AppSocket {

    /**
     * @type {AppSocket} socket
     */
    static appSocket;
    /**
     * @private
     * @type {io}
     */
    io;


    static getInstance() {
        if (!AppSocket.appSocket) {
            AppSocket.appSocket = new AppSocket();
        }
        return AppSocket.appSocket;
    }

    startListening(server) {
        this.io = socket(server, {
            cors: {
                origin: '*',
            }
        });
        this.addMiddlewareGuard();
        this.io.on(SocketEvent.CONNECT, (socket) => {
            //1- send new message to socket user with current users.
            const users = this.getCurrentLiveUsers();
            this.emitData(SocketEvent.LIVE_USERS, users);
            //2- send cached messages to newly connected user.
            MessageCache.getMessages().then((messages) => {
                this.emitData(SocketEvent.MESSAGES, messages);
            });

            socket.on(SocketEvent.DISCONNECT, () => {
                this.emitData(SocketEvent.USER_DISCONNECT, socket.user);
            });
            socket.on(SocketEvent.NEW_MESSAGE, (data) => {
                // save message and send it to all users.
                this.saveNewMessage(socket.user, data).then((message) => {
                    this.emitData(SocketEvent.NEW_MESSAGE, message);
                    MessageCache.saveMessage(message);
                });
            });
        });
    }


    addMiddlewareGuard = () => {
        this.io.use(async (socket, next) => {
            const authToken = socket.request.headers.authorization;
            if (authToken) {
                if (!socket.user) {
                    // verify user auth before connect to socket.
                    try {
                        socket.user = await JwtHelper.verify(authToken);
                        next();
                    } catch (e) {
                        AppLogger.logError(e);
                        let err = new Error('Authentication error');
                        err.data = {type: 'authentication_error'};
                        next(err);
                    }
                }
            } else {
                socket.conn.close();
            }
        });
    }

    emitData = (key, message) => {
        this.io.emit(key, message);
    }

    saveNewMessage = async (user, messageContent) => {
        const message = new Message({
            fromUserId: user._id,
            fromUserName: user.name,
            message: messageContent,
            createdAt: new Date()
        });
        return await MessageRepository.saveMessage(message);
    }

    getCurrentLiveUsers = () => {
        const users = [];
        this.io.sockets.sockets.forEach((socket) => {
            users.push(socket.user);
        });
        return users;
    }
}
const ApiResponseCode = require("../../../../constant/ApiResponseCode");
const AuthValidator = require("./validation/AuthValidator");
const User = require("../../../../data/model/User");
const BadRequestException = require("../../../exception/BadRequestException");
const EventObserver = require("../../../../util/EventObserver");
const Events = require("../../../../constant/Events");
const RandomGenerator = require("../../../../util/RandomGenerator");
const AppEnvironment = require("../../../../util/env/AppEnvironment");
const ApiResponse = require("../../../../util/response/ApiResponse");
const EncryptHelper = require("../../../../util/EncryptHelper");
const JwtHelper = require("../../../../util/JwtHelper");
const userRepository = new (require("../../../../data/repository/UserRepository"))();
const HttpResponse = require('../../../../util/response/HttpResponse');
const HttpStatusCode = require('../../../../constant/HttpStatusCode');

class AuthController {

    async register(req, res, next) {
        try {
            AuthValidator.validateRegisterData(req.body);
            const user = new User(req.body);
            const isEmailExistsBefore = await userRepository.isEmailExistsBefore(user.email);
            if (isEmailExistsBefore) {
                throw new BadRequestException("User with same email exists before");
            }
            // save user.
            user.emailTempToken = RandomGenerator.generate(40);
            user.createdAt = new Date();
            const newlyCreatedUser = await userRepository.saveUser(user);
            const emailData = {
                email: newlyCreatedUser.email,
                url: AppEnvironment.getEnv().emailUrl + "?token=" + newlyCreatedUser.emailTempToken
            };
            EventObserver.getInstance().sendEvent(Events.SEND_VERIFICATION_EMAIL, emailData);
            return ApiResponse.sendSuccess(req, res, "User registered successfully");
        } catch (e) {
            next(e);
        }
    }

    async login(req, res, next) {
        try {
            AuthValidator.validateLoginData(req.body);
            const user = new User(req.body);
            const dbUser = await userRepository.findUserBy({email: user.email});
            if (!dbUser) {
                throw new BadRequestException("User not exists");
            }
            const isPasswordMatch = await EncryptHelper.isMatch(user.password, dbUser.password);
            if (!isPasswordMatch) {
                throw new BadRequestException("User not exists");
            }

            // add user JWT token before return response.
            delete dbUser.password;
            dbUser.token = await JwtHelper.sign(dbUser.toJson());
            return ApiResponse.sendSuccess(req, res, `welcome ${dbUser.name}`, "user", dbUser);
        } catch (e) {
            next(e);
        }
    }

    async verifyEmail(req, res, next) {
        try {
            AuthValidator.validateMailVerification(req.body);
            const {verificationCode, newPassword} = req.body;
            const dbUser = await userRepository.findUserBy({emailTempToken: verificationCode});
            if (!dbUser) {
                throw new BadRequestException("User not exists");
            }
            const encryptedPassword = await EncryptHelper.hash(newPassword);
            const updatedData = {isVerified: true, password: encryptedPassword, emailTempToken: null};
            await userRepository.updateUser(dbUser._id, updatedData);
            return ApiResponse.sendSuccess(req, res, "Email verified successfully");
        } catch (e) {
            next(e);
        }
    }

    /**
     * get user profile test method to return response from HttpResponse builder method
     * @param req
     * @param res
     * @param next
     * @return {Promise<void>}
     */
    async getProfile(req, res, next) {
        const user = req.user;
        return (new HttpResponse(res)).setStatusCode(ApiResponseCode.SUCCESS)
            .setServerCode(HttpStatusCode.SUCCESS).setDataWithKey("user", user).send();
    }
}

module.exports = AuthController;
const RequestDataValidator = require("../../../../../util/validation/RequestDataValidator");
const authValidationSchema = require('./authValidationSchema');

class AuthValidator {

    /**
     * validate request body data to satisfy register user schema.
     * @param {any} data  - data that will be validated.
     * @throws {ValidationException} - if exception occurs validate method will throw exception and middleware will return
     * appropriate response message.
     */
    static validateRegisterData(data) {
        RequestDataValidator.validate(data, authValidationSchema.registerSchema);
    }

    /**
     * validate request body data to satisfy login user schema.
     * @param {any} data  - data that will be validated.
     * @throws {ValidationException} - if exception occurs validate method will throw exception and middleware will return
     * appropriate response message.
     */
    static validateLoginData(data) {
        RequestDataValidator.validate(data, authValidationSchema.loginSchema);
    }

    /**
     * validate request body data to satisfy mail verification schema.
     * @param {any} data  - data that will be validated.
     * @throws {ValidationException} - if exception occurs validate method will throw exception and middleware will return
     * appropriate response message.
     */
    static validateMailVerification(data) {
        RequestDataValidator.validate(data, authValidationSchema.mailVerification);
    }
}

module.exports = AuthValidator;
const Joi = require("joi");

module.exports = {
    registerSchema: Joi.object({
        name: Joi.string().required().min(4),
        email: Joi.string().email({tlds: {allow: false}}).required()
    }).unknown(true),
    loginSchema: Joi.object({
        email: Joi.string().email({tlds: {allow: false}}).required(),
        password: Joi.string().min(6).required(),
    }).unknown(true),
    mailVerification: Joi.object({
        verificationCode: Joi.string().required(),
        newPassword: Joi.string().min(6).required()
    }).unknown(true)
}
const nodemailer = require('nodemailer');
const AppEnvironment = require("../../util/env/AppEnvironment");
const AppLogger = require("../../util/log/AppLogger");

class MailSender {

    /**
     * Mail Receiver event callback method .
     * @param {object} data - data that sent from observer .
     */
    static sendEmail(data) {
        MailSender.sendEmailVerificationMail(data);
    }


    static async sendEmailVerificationMail(data) {
        try {
            let content = `
        <div>
        <h4>Email Verification</h4>
        <p>Please use this link to verify your email :- ${data.url}</p>
        </div>
        `;
            await MailSender.send(data.email, "Verify Account", content)
        } catch (e) {
            AppLogger.logError(e);
        }
    }

    static async send(receiver, subject, content) {
        const transporter = await MailSender.initTransporter();
        let info = await transporter.sendMail({
            from: '"LinkDevelopment" <help.linkdevelopment.com>',
            to: receiver,
            subject: subject,
            html: content,
        });
        if (!info.messageId) {
            AppLogger.log(info.response);
        }
    }

    static async initTransporter() {
        const appEnv = AppEnvironment.getEnv();
        const appEmail = appEnv.appEmail;
        const appEmailPassword = appEnv.appEmailPassword;
        return nodemailer.createTransport({
            service: 'Gmail',
            port: 587,
            secure: false,
            auth: {
                user: appEmail,
                pass: appEmailPassword,
            },
        });

    }
}

module.exports = MailSender;
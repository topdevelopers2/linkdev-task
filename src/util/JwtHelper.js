const jwt = require("jsonwebtoken");
const AppEnvironment = require("./env/AppEnvironment");
const NotAuthorizedException = require("../http/exception/NotAuthorizedException");

class JwtHelper {

    /**
     *
     * @param {any} payload  - data to sign and write it in jwt payload.
     * @param {number} expiresInSeconds - millis that token will be alive before expired , default 1 hour.
     */
    static sign(payload, expiresInSeconds = (60 * 60)) {
        return new Promise((resolve, reject) => {
            jwt.sign(payload, AppEnvironment.getEnv().jwtSecretToken, {expiresIn: expiresInSeconds}, function (err, token) {
                if (err) {
                    reject(err);
                } else {
                    resolve(token);
                }
            });
        });
    }

    /**
     *
     * @param {string} token - user token
     * @returns {Promise<User>} - jwt verify result.
     */
    static async verify(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, AppEnvironment.getEnv().jwtSecretToken, (err, user) => {
                if (err) {
                    reject(new NotAuthorizedException("User not authorized"));
                } else {
                    resolve(user);
                }
            });
        });
    }
}

module.exports = JwtHelper;
const ValidationException = require("../../http/exception/ValidationException");

class RequestDataValidator {

    /**
     * validate request body data.
     * @param {object} data - data to validate.
     * @param {object} schema - validation schema to test it on data.
     */
    static validate(data, schema) {
        const validationResult = schema.validate(data);
        if (validationResult.error) {
            throw new ValidationException(validationResult.error.message);
        }
    }
}

module.exports = RequestDataValidator;
class AppEnv {

    /**
     * @type {string} env
     */
    env;
    /**
     *@type {string} dbUrl
     */
    dbUrl;
    /**
     *@type {number} port
     */
    port;
    /**
     * @type {boolean} enableLog
     */
    enableLog;

    /**
     * @type {string}
     */
    appEmail;

    /**
     * @type {string}
     */
    appEmailPassword

    /**
     * @type {string}
     */
    jwtSecretToken;
    /**
     * @type {string}
     */
    redisHost;
    /**
     * @type {number}
     */
    redisPort;

    /**
     *
     * @param {object} processEnv
     */
    constructor(processEnv) {
        this.env = processEnv['ENV'];
        this.dbUrl = processEnv['DB_URL'];
        this.port = parseInt(processEnv['PORT']);
        this.enableLog = processEnv['ENABLE_LOG'];
        this.appEmail = processEnv['APP_EMAIL'];
        this.appEmailPassword = processEnv['APP_EMAIL_PASSWORD'];
        this.jwtSecretToken = processEnv['JWT_SECRET_TOKEN'];
        this.redisHost = processEnv['REDIS_HOST'];
        this.redisPort = parseInt(processEnv['REDIS_PORT']);
        this.appUrl = processEnv['APP_URL'];
        this.emailUrl = processEnv['APP_EMAIL_URL'];
    }

    isProductionEnv() {
        return this.env === "prod";
    }

    isTestEnv() {
        return !this.isProductionEnv();
    }
}

module.exports = AppEnv;
require('dotenv').config();
const AppEnv = require("./AppEnv");

class AppEnvironment {
    /**
     * @private
     * @type {AppEnv}
     */
    static appEnv;

    static init() {
        if (AppEnvironment.appEnv == null) {
            AppEnvironment.appEnv = new AppEnv(process.env);
        }
    }

    /**
     *
     * @returns {AppEnv}
     */
    static getEnv() {
        return AppEnvironment.appEnv;
    }

    /**
     *
     * @param {string} key
     * @param {any} defaultValue - default value if value not exists in env data object.
     */
    static getEnvValue(key, defaultValue = null) {
        const appEnv = AppEnvironment.getEnv();
        if (appEnv) {
            return appEnv[key] || defaultValue;
        }
        return defaultValue;
    }

}

module.exports = AppEnvironment;
const bcrypt = require("bcrypt");

module.exports = class EncryptHelper {

    /**
     * encrypt password
     * @param {string} password - password to encrypt.
     * @return {Promise<string>} - newly hashed password.
     */
    static async hash(password) {
        return bcrypt.hash(password, 10);
    }

    /**
     * check if hashed password match user entered plain password.
     * @param {string} plainPassword  - user entered plain password.
     * @param {string} hashedPassword - saved hashed password.
     * @return {void|Promise<boolean>} - return true if match otherwise false.
     */
    static isMatch(plainPassword, hashedPassword) {
        return bcrypt.compare(
            plainPassword,
            hashedPassword
        );
    }
}
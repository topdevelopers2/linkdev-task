const AppEnvironment = require("../env/AppEnvironment");

class AppLogger {

    static log(data) {
        if (AppLogger.enableLog()) {
            if (data instanceof Array) {
                console.log(...data);
            } else {
                console.log(data);
            }
        }
    }

    static logError(error) {
        if (AppLogger.enableLog()) {
            if (error instanceof Array) {
                console.error(...error);
            } else {
                console.error(error);
            }
        }
    }

    /**
     * @private
     */
    static enableLog() {
        return AppEnvironment.getEnv().enableLog;
    }
}

module.exports = AppLogger;
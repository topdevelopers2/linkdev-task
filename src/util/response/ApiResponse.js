const ApiResponseCode = require("../../constant/ApiResponseCode");
const BaseException = require("../../http/exception/BaseException");

class ApiResponse {


    static sendSuccess(req, res, message = "", dataKey, data, statusCode = ApiResponseCode.SUCCESS, serverCode = 200) {
        const response = {};
        response.statusCode = statusCode;
        if (dataKey) {
            response[dataKey] = data;
        } else {
            response.data = null;
        }
        response.message = message;
        return res.status(serverCode).json(response);
    }

    static sendFail(req, res, message = "", statusCode = ApiResponseCode.FAIL, serverCode = 400, dataKey, data) {
        const response = {};
        response.statusCode = statusCode;
        if (dataKey) {
            response[dataKey] = data;
        } else {
            response.data = null;
        }
        response.message = message;
        return res.status(serverCode).json(response);
    }

    static sendException(req, res, exception) {
        const response = {};
        response.data = null;
        if (exception instanceof BaseException) {
            response.statusCode = exception.statusCode;
            response.serverCode = exception.serverCode;
        } else {
            response.statusCode = ApiResponseCode.SERVER_ERROR;
            response.serverCode = 500;
        }
        response.message = exception.message || "";
        return res.status(response.serverCode).json(response);
    }


    /**
     *
     * @param req
     * @param res
     * @param {any} options
     * @returns {*}
     */
    static sendFromOptions(req, res, options) {
        const statusCode = options.statusCode || ApiResponseCode.SUCCESS;
        const serverCode = options.serverCode || 200;
        const data = options.data || null;
        const dataKey = options.dataKey || "data";
        const message = options.message || "";
        return ApiResponse.send(req, res, dataKey, data, statusCode, serverCode, message);
    }


}

module.exports = ApiResponse;
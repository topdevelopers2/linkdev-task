const ApiResponseCode = require("../../constant/ApiResponseCode");
const BaseException = require("../../http/exception/BaseException");
module.exports = class HttpResponse {

    /**
     * @private
     * @type{string}
     */
    statusCode;
    /**
     * @private
     * @type {number}
     */
    serverCode;

    /**
     * @private
     * @type {string}
     */
    message;
    /**
     * @private
     * @type {string}
     */
    dataKey;
    /**
     * @private
     * @type {string}
     */
    data;

    /**
     * @type {response}
     */
    response;

    constructor(res) {
        this.response = res;
    }

    /**
     *
     * @param statusCode
     * @return {HttpResponse}
     */
    setStatusCode(statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    /**
     * @param {number} serverCode
     * @return {HttpResponse}
     */
    setServerCode(serverCode) {
        this.serverCode = serverCode;
        return this;
    }

    /**
     *
     * @param {string} key
     * @return {HttpResponse}
     */
    setDataKey(key) {
        this.dataKey = key;
        return this;
    }

    /**
     * @param {string} dataKey
     * @param {string} data
     * @return {HttpResponse}
     */
    setDataWithKey(dataKey, data) {
        this.dataKey = dataKey;
        this.data = data;
        return this;
    }

    /**
     *
     * @param data
     * @return {HttpResponse}
     */
    setData(data) {
        this.data = data;
        return this;
    }

    send() {
        const response = {};
        response.statusCode = this.statusCode;
        if (this.dataKey) {
            response[this.dataKey] = this.data;
        } else {
            response.data = null;
        }
        response.message = this.message;
        return this.response.status(this.serverCode).json(response);
    }

    sendError(exception) {
        const response = {};
        response.data = null;
        if (exception instanceof BaseException) {
            response.statusCode = exception.statusCode;
            response.serverCode = exception.serverCode;
        } else {
            response.statusCode = ApiResponseCode.SERVER_ERROR;
            response.serverCode = 500;
        }
        response.message = exception.message || "";
        return this.response.status(response.serverCode).json(response);
    }


}
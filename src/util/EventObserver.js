const events = require('events');
const eventEmitter = new events.EventEmitter();

class EventObserver {
    /**
     * @type {EventObserver}
     */
    static observer;
    /**
     * @private
     * @type {object}
     */
    listeners;

    /**
     * @private
     */
    constructor() {
        this.listeners = {};
    }

    static getInstance() {
        if (EventObserver.observer == null) {
            EventObserver.observer = new EventObserver();
        }
        return EventObserver.observer;
    }

    /**
     *
     * @param {string} eventName - event name to listen
     * @param {function} callbackMethod - function to call if event occurred.
     */
    subscribe(eventName, callbackMethod) {
        this.listeners[eventName] = callbackMethod;
        eventEmitter.addListener(eventName, callbackMethod);
    }

    /**
     *
     * @param {string} eventName - remove event from listeners with callback method.
     */
    unsubscribe(eventName) {
        eventEmitter.removeListener(eventName, this.listeners[eventName]);
    }

    /**
     * @param {string} eventName
     * @param {*} data - data to send to event callback method.
     */
    sendEvent(eventName, data) {
        eventEmitter.emit(eventName, data);
    }

}

module.exports = EventObserver;
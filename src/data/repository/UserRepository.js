const DbConnection = require("../db/DbConnection");
const User = require("../model/User");
module.exports = class UserRepository {

    /**
     * check if email exists before in users collection.
     * @param {string}  email - email that will be checked for existence in db.
     * @returns {Promise<boolean>} - true if email exists otherwise false.
     */
    async isEmailExistsBefore(email) {
        const userWithSameEmail = await DbConnection.getDb().collection("Users").findOne({email}, {email: 1});
        return userWithSameEmail != null;
    }

    /**
     * save new user in db
     * @param {User} user - user that will be saved.
     * @returns {Promise<User>}  - return newly created user.
     */
    async saveUser(user) {
        const newlyCreatedUser = await DbConnection.getDb().collection("Users").insertOne(user.toJson());
        return new User(newlyCreatedUser.ops[0]);
    }

    /**
     *
     * @param {object} criteria - filter criteria.
     * @param {object} selection - selection fields for retrieval
     * @return {Promise<User>} - user or null.
     */
    async findUserBy(criteria, selection = {}) {
        const userData = await DbConnection.getDb().collection('Users').findOne(criteria, selection);
        return userData ? new User(userData) : null;
    }

    /**
     * update user info.
     * @param {string} userId - user unique id.
     * @param {object} updatedData - data want to update in user doc.
     * @return {Promise<void>}
     */
    async updateUser(userId, updatedData) {
        await DbConnection.getDb().collection("Users").updateOne({_id: userId}, {$set: updatedData});
    }
}
const Message = require("../model/Message");
const DbConnection = require("../db/DbConnection");
module.exports = class MessageRepository {

    /**
     * save new message in db.
     * @param {Message} message
     */
    static async saveMessage(message) {
        const newlyCreatedMessage = await DbConnection.getDb().collection("Messages").insertOne(message.toJson());
        return new Message(newlyCreatedMessage.ops[0]);
    }
}
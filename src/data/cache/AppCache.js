const redis = require('redis');
const AppEnvironment = require("../../util/env/AppEnvironment");
const AppLogger = require("../../util/log/AppLogger");

class AppCache {

    static redisClient;

    static connect() {
        const appEnv = AppEnvironment.getEnv();
        AppCache.redisClient = redis.createClient(appEnv.redisPort, appEnv.redisHost);
        AppCache.redisClient.on('connect', function () {
            AppLogger.log("connected to redis DB");
        });
    }

    /**
     *
     * @param {string} key - key of data that want to save in redis DB.
     * @param {string} value - value of that key.
     */
    static save(key, value) {
        AppCache.redisClient.set(key, value);
    }

    /**
     *
     * @param {string} key - saved data key.
     * @param {any} defaultValue - default value if data not found.
     * @returns {Promise<any>} - data if key value exists or default value.
     */
    static async get(key, defaultValue = null) {
        return new Promise((resolve, reject) => {
            AppCache.redisClient.get(key, (err, value) => {
                resolve((value || defaultValue));
            });
        });
    }

    /**
     *
     * @param {string} key - check if cache has key.
     * @returns {Promise<boolean>} - return true if key exists otherwise false.
     */
    static async contains(key) {
        return new Promise((resolve, reject) => {
            AppCache.redisClient.exists(key, function (err, reply) {
                if (reply === 1) { // exists.
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }

    /**
     *
     * @param {string} key - remove key with value from cache.
     */
    static remove(key) {
        AppCache.redisClient.del(key);
    }
}

module.exports = AppCache;
const AppCache = require("./AppCache");

class MessageCache {

    static async saveMessage(newMessage) {
        let messages = await MessageCache.getMessages();
        if (messages.length === 10) {
            // reach max messages that saved in cache , remove last old message and push new message instead of.
            MessageCache.unshiftMessages(newMessage, messages);
        } else {
            messages.push(newMessage);
        }
        AppCache.save("CHAT_MESSAGES", JSON.stringify(messages));
    }

    static async getMessages() {
        const messages = await AppCache.get("CHAT_MESSAGES");
        if (messages) {
            return JSON.parse(messages);
        }
        return [];
    }

    /**
     * push new message in last array elements and move old messages on step to remove old message.
     * @param {any} newMessage - message to add in last position of messages.
     * @param {array} messages - messages array to unshift it's items and remove old one and add new one instead of.
     */
    static unshiftMessages(newMessage, messages) {
        for (let i = 0; i < messages.length; i++) {
            if (i < messages.length - 1) {
                messages[i] = messages[(i + 1)];
            } else {
                messages[i] = newMessage;
            }
        }
    }
}

module.exports = MessageCache;
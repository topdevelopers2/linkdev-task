const AppEnvironment = require("../../util/env/AppEnvironment");
const AppLogger = require("../../util/log/AppLogger");
const MongoClient = require('mongodb').MongoClient;


class DbConnection {

    /**
     * @private
     *
     */
    static database;

    static connect() {
        MongoClient.connect(AppEnvironment.getEnv().dbUrl, {useUnifiedTopology: true, useNewUrlParser: true},
            function (err, connection) {
                if (err) {
                    // log error and send mail .
                    AppLogger.logError(err);
                } else {
                    AppLogger.log("connected to mongo db");
                    DbConnection.database = connection.db('linkdev');
                }
            });
    }

    static getDb() {
        return DbConnection.database;
    }
}

module.exports = DbConnection;
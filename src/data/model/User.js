module.exports = class User {
    constructor(data) {
        this._id = data['_id'];
        this.name = data['name'];
        this.email = data['email'];
        this.password = data['password'];
        this.isVerified = data['isVerified'] || false;
        this.createdAt = data['createdAt'];
        this.emailTempToken = data['emailTempToken'];
    }

    toJson = () => ({
        _id: this._id,
        name: this.name,
        email: this.email,
        password: this.password,
        isVerified: this.isVerified,
        createdAt: this.createdAt,
        emailTempToken: this.emailTempToken
    });
}
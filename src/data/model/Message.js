module.exports = class Message {
    constructor(data) {
        this._id = data['_id'];
        this.fromUserId = data['fromUserId'];
        this.fromUserName = data['fromUserName'];
        this.message = data['message'];
        this.createdAt = data['createdAt'];
    }


    toJson() {
        return {
            _id: this._id,
            fromUserId: this.fromUserId,
            fromUserName: this.fromUserName,
            message: this.message,
            createdAt: this.createdAt
        };
    }
}